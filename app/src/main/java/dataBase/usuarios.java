package dataBase;

import java.io.Serializable;

public class usuarios implements Serializable {

    private long _ID;
    private String nombre;
    private String contrasena;
    private String Repicontrasena;

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }

    private String Usuario;


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setContrasena(String contraseña) {
        this.contrasena = contraseña;
    }

    public void setRepicontrasena(String CONTRASEÑA2) {
        this.Repicontrasena = CONTRASEÑA2;
    }

    public usuarios() {
        this._ID = 0;
        this.nombre = "";
        this.contrasena = "";
        this.Repicontrasena = "";
        this.Usuario = "";
    }

    public usuarios( int _ID, String nombre, String usuario, String contrasena1, String contrasena2) {
        this._ID = _ID;
        this.nombre = nombre;
        this.Usuario = usuario;
        this.contrasena = contrasena1;
        this.Repicontrasena = contrasena2;
    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public String getContrasena() {
        return contrasena;
    }

    public String getRepicontrasena() {
        return Repicontrasena;
    }


}



