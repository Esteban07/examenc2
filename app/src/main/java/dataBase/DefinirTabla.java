package dataBase;

import android.provider.BaseColumns;

public class DefinirTabla  {

    public DefinirTabla(){}

    public static abstract class Usuario implements BaseColumns {
        public static final String TABLA_NAME = "Usuario";
        public static final String COLUMN_NAME_USUARIO = "usuario";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_CONTRASENA = "Contraseña";
        public static final String COLUMN_NAME_CONTRASENA2 = "Contraseña2";
    }
}
