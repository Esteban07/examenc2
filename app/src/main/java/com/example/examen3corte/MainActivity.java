package com.example.examen3corte;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import dataBase.AgendaUsuario;
import dataBase.usuarios;

public class MainActivity extends AppCompatActivity {

    private EditText lblUsuario;
    private EditText lblContraseña;
    private Button btnIngresar;
    private Button btnRegistrarse;

    private int id;
    private AgendaUsuario db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        lblUsuario = (EditText) findViewById(R.id.lblNombreUsuario);
        lblContraseña = (EditText) findViewById(R.id.lblContraseñaUsuario);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnRegistrarse = (Button) findViewById(R.id.btnRegistrarse);


        db = new AgendaUsuario(MainActivity.this);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lblUsuario.getText().toString().equals("") || lblContraseña.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Por favor ingrese todos los datos para ingresar", Toast.LENGTH_SHORT).show();
                } else {
                    db.openDatabse();
                    usuarios usuario = db.getUsuario(lblUsuario.getText().toString(), lblContraseña.getText().toString());
                    if (usuario == null) {
                        Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, IngresoActivity.class);
                        intent.putExtra("nombre", usuario.getNombre());
                        startActivityForResult(intent, 0);
                    }
                    db.cerrar();
                }
            }
        });

        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivityForResult(intent, 0);
            }
        });

    }
}
