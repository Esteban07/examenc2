package dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaUsuario {
    private Context context;
    private AgendaDbHelper agendaDbHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[] {
            DefinirTabla.Usuario._ID,
            DefinirTabla.Usuario.COLUMN_NAME_USUARIO,
            DefinirTabla.Usuario.COLUMN_NAME_NOMBRE,
            DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA,
            DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA2,
    };

    public AgendaUsuario(Context context) {
        this.context = context;
        this.agendaDbHelper = new AgendaDbHelper(this.context);
    }
    public void openDatabse() {db = agendaDbHelper.getWritableDatabase();}

    public long insertarUsuarios (usuarios u){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Usuario.COLUMN_NAME_NOMBRE, u.getNombre());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_USUARIO, u.getUsuario());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA, u.getContrasena());

        return db.insert(DefinirTabla.Usuario.TABLA_NAME, null, values);
    }


    public  usuarios leerUsuario(Cursor cursor) {
        usuarios u = new usuarios();

        u.set_ID(cursor.getInt(0));
        u.setUsuario(cursor.getString(1));
        u.setNombre(cursor.getString(2));
        u.setContrasena(cursor.getString(3));


        return u;
    }



    public usuarios getUsuario(String user, String clave){
        usuarios usuario = null;

        SQLiteDatabase db = this.agendaDbHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Usuario.TABLA_NAME, columnToRead,
                DefinirTabla.Usuario.COLUMN_NAME_USUARIO + " = ? AND " +
                        DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA + " = ? ", new String[] {String.valueOf(user), String.valueOf(clave)}, null, null, null);
        if (cursor.moveToFirst()){
            usuario = leerUsuario(cursor);
        }
        cursor.close();
        return usuario;
    }

    public void cerrar() { agendaDbHelper.close();}
}

